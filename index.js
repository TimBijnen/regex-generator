// addEventListener('load', onLoad);

// function onLoad(e) {
// document.querySelector('#test').innerHTML = data.map(str => {
//   console.log(str)
//   return '<div><h5>' + str + '</h5><p>' + toRegex(str) + '</p></div>'
// }).join('<hr>')
// }

var data = [
  'nl52 ingb 7493 2837 93',
  'nl57ingb2938402938',
  '62849284',
  'hm7390',
  'F0407',
  '11-02-1988',
];

var NUMBER = { number: true };
var LETTER = { letter: true };
var SPACE = { space: true };
var DASH = { dash: true };

function toGroup(a) {
  let groupIndex = 0;
  var groups = [[]];

  a.split('').forEach(val => {
    var space = isSpace(val);
    var type;

    if (space) {
      groupIndex += 1;
      groups.push([]);
      return false;
    }
     
    if (isNumber(val)) {
      type = NUMBER;
    } else if (isLetter(val)) { 
      type = LETTER;
    } else {
      type = DASH;
    }

    return groups[groupIndex].push({
      val,
      type,
    })
  });

  return groups;
}

function isNumber(a) {
  return !Number.isNaN(parseInt(a));
}

function isLetter(a) {
  return 'abcdefghijklmnopqrstuvwxyz'.indexOf(a.toLowerCase()) >= 0;
}

function isSpace(a) {
  return a === ' ';
}

function toRegex(a) {
  var grouped = toGroup(a);
  var regex = '';
  var pairs = [];
  var pairIndex = 0;

  grouped.forEach((group, i) => {
    var prevType;
    group.forEach(o => {
      var pair = pairs[pairIndex];

      if (!pair) {
        pair = { type: o.type, amt: 1 };
        pairs.push(pair);
      } else if (o.type === prevType) {
        pair.amt += 1;
      } else {
        pair = { type: o.type, amt: 1 };
        pairs.push(pair);
        pairIndex += 1;
      }
       
      prevType = o.type;
    });
    if (i !== grouped.length - 1) {
      pairs.push({ type: SPACE });
      pairIndex += 1;
    }
  });

  pairs.forEach(pair => {
    if (pair.type === NUMBER) {
      regex += '\\d'     
    } else if (pair.type === SPACE) {
      regex += '\\s+';
    } else if (pair.type === LETTER) {
      regex += '[a-zA-Z]';
    } else if (pair.type === DASH) {
      regex += '-';
    }
    if (pair.amt > 1) {
      regex += '{' + pair.amt + '}';
    }
  })

  return regex;
}